import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Index',
    component: () => import('../views/index.vue')
  },
  {
    path: '/about-us',
    name: 'aboutUs',
    component: () => import('../views/aboutUs.vue')
  },
  {
    path: '/politique-de-confidentialite',
    name: 'politiqueDeConfidentialite',
    component: () => import('../views/confidentiality.vue')
  },
  {
    path: '/cgv',
    name: 'cgv',
    component: () => import('../views/cgv.vue')
  },
  {
    path: '/cgu',
    name: 'cgu',
    component: () => import('../views/cgu.vue')
  },
  {
    path: "/login",
    name: "Login page",
    component: () => import('../views/login.vue')
  },
  {
    path: "/register",
    name: "Signup page",
    component: () => import('../views/register/signup.vue')
  },
  {
    path: "/register/freelance",
    name: "Signup freelance",
    component: () => import('../views/register/freelancer/signup.vue')
  },
  {
    path: "/register/enterprise",
    name: "Signup enterprise",
    component: () => import('../views/register/enterprise/signup.vue')
  },
  {
    path: '/forgot-password',
    name: 'Forgot Password page',
    component: () => import('../views/forgot-password.vue')
  },
  {
    path: '/reset-password',
    name: 'Reset Password page',
    component: () => import('../views/reset-password.vue')
  },
  {
    path: '/validation-account',
    name: 'validation_account',
    component: () => import('../views/validation-user.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
