FROM node:16 as vue-builder
WORKDIR /app
COPY . /app/
RUN npm install && npm run build

FROM nginx:latest
RUN mkdir /app
COPY --from=vue-builder /app/dist /app
COPY nginx.conf /etc/nginx/nginx.conf
